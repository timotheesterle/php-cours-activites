﻿<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Les emails</title>
</head>
<body>
<table>
    <thead>
        <tr>
            <td>Email</td>
            <td>Date</td>
        </tr>
    </thead>
    <tbody>

    <?php
    $emails = json_decode(file_get_contents('./emails.json'), true);
    foreach ($emails as $email):
    ?>

        <tr>
            <td><?= $email['email'] ?></td>
            <td><?= $email['date'] ?></td>
        </tr>

    <?php endforeach; ?>

    </tbody>
</table>
</body>
</html>
