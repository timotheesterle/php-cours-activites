<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>PHP & HTML</title>
    </head>
    <body>
        <h1>Liste des élèves</h1>
        <!-- Instructions : Afficher la liste des éléves qui sont présent dans le tableau $students -->
        <?php
        //students
        $students = ['Hulk', 'Iron Man', 'Wonder Woman', 'Black Widow', 'Malicia'];
     ?>
        <ul>
            <?php foreach ($students as $student) { ?>
            <li><?= $student ?></li>
            <?php } ?>
        </ul>
        <hr />
        <h1>Date du jour</h1>
        <form>
            <!-- Instructions : Créer la liste de jour (en chiffres), de mois (en chiffres) et d'année en PHP. -->
            <label for="day">Day</label>
            <select name="day">
                <?php foreach (range(1,31) as $day) { ?>
                <option value="<?= $day ?>"><?= $day ?></option
                >
                <?php } ?>
            </select>
            <label for="month">Month</label>
            <select name="month">
                <?php foreach (range(1,12) as $month) { ?>
                <option value="<?= $month ?>"><?= $month ?></option
                >
                <?php } ?>
            </select>
            <label for="year">Year</label>
            <select name="year">
                <?php foreach (range(1990,2020) as $year) { ?>
                <option value="<?= $year ?>"><?= $year ?></option
                >
                <?php } ?>
            </select>
        </form>
        <hr />
        <?php if ($_GET["sexe"] === "fille") { ?>
        <!-- Instruction : Afficher ce bloc que si dans l'URL il y'a une variable sexe et que ça valeur vaut "fille" -->
        <p>
            Je suis une fille
        </p>
        <?php } elseif ($_GET["sexe"] === "garçon") { ?>
        <!-- Instruction : Afficher ce bloc que si dans l'URL il y'a une variable sexe et que ça valeur vaut "garçon" -->
        <p>
            Je suis un garçon
        </p>
        <!-- Instruction : Afficher ce bloc dans les autres cas -->
        <?php } else { ?>
        <p>
            Je suis indéfini
        </p>
        <?php } ?>
    </body>
</html>
