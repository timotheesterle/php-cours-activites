<?php

/*****************************************/
/********** Les boucles : For ************/
/*****************************************/
echo '1.Nombre de mouton : <br>';
for ($i = 0; $i <= 10; ++$i) {
    echo $i.' mouton(s)<br>';
}
echo '<br><br>';
//----------------------------------------
//for : particulièrement utile pour pacourir un tableau
$couleurs = ['rouge', 'bleu', 'vert', 'orange', 'marron', 'noir', 'blanc'];
//count est une fonction proposée par php qui sert à compter le nombre d'éléments d'un tableau
echo '2.Les couleurs : <br>';
for ($i = 0; $i < count($couleurs); ++$i) {
    echo $couleurs[$i].'<br>';
}
echo '<br><br>';

//----------------------------------------
//Parcourir un tableau depuis la fin
echo '3.Les couleurs depuis la fin: <br>';
//n'oubliez pas qu'un tableau commence à l'index 0. C'est pour ça qu'on commence notre $i à la taille du tableau moins 1
for ($i = count($couleurs) - 1; $i >= 0; --$i) {
    echo $couleurs[$i].'<br>';
}
echo '<br><br>';
//----------------------------------------
//Parcourir un tableau multidimensionnel à 2 dimensions
$couleurs = array(
  array('rouge clair', 'rouge', 'rouge fonce'),
  array('bleu clair', 'bleu', 'bleu fonce'),
  array('vert clair', 'vert', 'vert fonce'),
  array('orange clair', 'orange', 'orange fonce'),
  array('marron clair', 'marron', 'marron fonce'),
);

echo '4.Les nuances de couleurs : <br>';
for ($i = 0; $i < count($couleurs); ++$i) {
    for ($j = 0; $j < count($couleurs[$i]); ++$j) {
        echo $couleurs[$i][$j].'<br>';
    }
}

echo '<br><br>';

/*****************************************/
/********** Les boucles : Foreach ********/
/*****************************************/
//parcourir un tableau simple
$couleurs = ['rouge', 'bleu', 'vert', 'orange', 'marron', 'noir', 'blanc'];
echo '5.Les couleurs : <br>';
foreach ($couleurs as $couleur) {
    echo $couleur.'<br>';
}
echo '<br><br>';

//----------------------------------------
//parcourir un tableau associatif
$vehicule = array(
  //clé => valeur
  'nom' => 'Aventador LP 700-4',
  'marque' => 'Lamborghini',
  'puissance' => 700,
  'prix' => 200000,
);
echo '6.Specificite de ma voiture : <br>';
//syntax : foreach($tableau as $cle => $valeur )
foreach ($vehicule as $propriete => $valeur) {
    echo $propriete.':'.$valeur.'<br>';
}
echo '<br><br>';

//----------------------------------------
//parcourir un tableau associatif multidimensionnel
$vehiculeConcession = array(
  'Bas de gamme' => array(
    'nom' => 'C1',
       'marque' => 'Citroen',
       'puissance' => 70,
       'prix' => 10000,
  ),
  'Milieu de gamme' => array(
    'nom' => 'Golf',
       'marque' => 'VW',
       'puissance' => 140,
       'prix' => 270000,
  ),
  'Haut de gamme' => array(
    'nom' => 'Aventador LP 700-4',
       'marque' => 'Lamborghini',
       'puissance' => 700,
       'prix' => 200000,
  ),
);
echo '7.Les voitures dans la concession :';
foreach ($vehiculeConcession as $gamme => $vehicule) {
    echo '<br>'.$gamme;
    foreach ($vehicule as $propriete => $valeur) {
        echo $propriete.' : '.$valeur.'<br>';
    }
}
echo '<br><br>';

/*###############################################*/
/*################# ACTIVITÉS ###################*/
/*###############################################*/

//----------------------------------------
//Créer un tableau pour les mois de l'année et affiché tous les mois de Janvier à Décembre
//modifier et/ou remplacer les éléments ci-dessous
echo "8.Les mois depuis le debut de l'annee : <br>";
$mois = [
    0 => 'Janvier',
    1 => 'Février',
    2 => 'Mars',
    3 => 'Avril',
    4 => 'Mai',
    5 => 'Juin',
    6 => 'Juillet',
    7 => 'Août',
    8 => 'Septembre',
    9 => 'Octobre',
    10 => 'Novembre',
    11 => 'Décembre'
];
for ($i = 0; $i < count($mois); $i++) {
    echo $mois[$i].'<br>';
}
echo '<br><br>';

//Afficher les mois de la fin de l'année jusqu'au début de l'année
//modifier et/ou remplacer les éléments ci-dessous
echo "9.Les mois depuis la fin de l'annee : <br>";
for ($i = count($mois); $i > 0; $i--) {
    echo $mois[$i].'<br>';
}
echo '<br><br>';
//----------------------------------------
//Afficher le nom et prénoms des élèves de ce collège
$college = array(
  'Sixieme' => array(
    array('Nom' => 'Payet', 'Prenom' => 'Mickael'),
    array('Nom' => 'Hoareau', 'Prenom' => 'Christine'),
    array('Nom' => 'Maillot', 'Prenom' => 'Laure'),
  ),
  'Cinquieme' => array(
    array('Nom' => 'Bourdon', 'Prenom' => 'Didier'),
    array('Nom' => 'Legitimus', 'Prenom' => 'Pascal'),
    array('Nom' => 'Campan', 'Prenom' => 'Bernard'),
    array('Nom' => 'Fois', 'Prenom' => 'Marina'),
    array('Nom' => 'Floresti', 'Prenom' => 'Florence'),
  ),
  'Quatrieme' => array(
    array('Nom' => 'Willis', 'Prenom' => 'Bruce'),
    array('Nom' => 'Lawrence', 'Prenom' => 'Laurence'),
    array('Nom' => 'Johannson', 'Prenom' => 'Scarlett'),
    array('Nom' => 'Jackson', 'Prenom' => 'Samuel'),
  ),
);

echo '10.Les eleves du college : <br>';
foreach ($college as $classe) {
    foreach ($classe as $eleve) {
        echo $eleve["Prenom"].' '.$eleve["Nom"].'<br>';
    }
}
echo '<br><br>';

//----------------------------------------
//Afficher le nom et prénoms des élèves de ce collège
//reprenez le tableau ci-dessus, ajoutez des éléves pour la classe de troisième et réaffichez tout
echo '11.Les eleves du college (avec les nouveaux arrivants): <br>';

$college["Troisieme"] = [
    ['Nom' => 'Egbert', 'Prenom' => 'John'],
    ['Nom' => 'Strider', 'Prenom' => 'Dave'],
    ['Nom' => 'Lalonde', 'Prenom' => 'Rose'],
    ['Nom' => 'Harley', 'Prenom' => 'Jade']
];

foreach ($college as $classe) {
    foreach ($classe as $eleve) {
        echo $eleve["Prenom"].' '.$eleve["Nom"].'<br>';
    }
}

echo '<br><br>';

//----------------------------------------
//Afficher toutes les informations de la vidéothèque
$videotheque = array(
  array(
    'nom' => 'Independance day',
    'date' => 1996,
    'realisateur' => 'Roland Emmerich',
    'acteurs' => array(
      'Will Smith', 'Bill Pullman', 'Jeff Goldblum', 'Mary McDonnell',
    ),
  ),
  array(
    'nom' => 'Bienvenue a Gattaca',
    'date' => 1998,
    'realisateur' => 'Andrew Niccol',
    'acteurs' => array(
      'Ethan Hawke', 'Uma Thurman', 'Jude Law',
    ),
  ),
  array(
    'nom' => 'Forrest Gump',
    'date' => 1994,
    'realisateur' => 'Robert Zemeckis',
    'acteurs' => array(
      	'Tom Hanks', 'Gary Sinise',
    ),
  ),
  array(
    'nom' => '12 hommes en colere',
    'date' => 1957,
    'realisateur' => 'Sidney Lumet',
    'acteurs' => array(
      	'Henry Fonda','Martin Balsam','John Fiedler','Lee J. Cobb','E.G. Marshall',
    ),
  ),
);

echo '12.Mes films : <br>';

foreach ($videotheque as $film) {
    foreach ($film as $cle => $info) {
        if (is_array($info)) {
            echo "$cle : ";
            foreach ($info as $item) {
                echo "$item, ";
            }
        } else {
            echo "$cle : $info<br>";
        }
    }
    echo "<br><br>";
}

echo '<br><br>';

//----------------------------------------
//Afficher toutes les informations de la vidéothèque
//reprenez le tableau ci-dessus, ajoutez-y 3 de vos films préférés avec les mêmes
//d'informations (nom, date, realisateur, acteurs) et en plus de ces informations
//rajoutez un synopsis

echo '13.Mes films : <br>';

$videotheque[] = [
    'nom' => 'L\'Orphelinat',
    'date' => 2007,
    'realisateur' => 'Juan Antonio Bayona',
    'synopsis' => "<p>Laura a passé son enfance dans un orphelinat entourée d'autres enfants qu'elle aimait comme ses frères et sœurs. Adulte, elle retourne sur les lieux avec son mari et son fils de sept ans, Simon, avec l'intention de restaurer la vieille maison et d'en faire un lieu d'accueil pour enfants handicapés. La demeure réveille l'imagination de Simon, qui commence à se livrer à d'étranges jeux avec « ses amis invisibles ». Le jour de l'inauguration du nouvel établissement, une dispute éclate entre Simon et Laura : Simon refuse de descendre pour accueillir les nouveaux arrivants (les enfants handicapés) ; refusant de céder à un caprice, Laura laisse Simon seul et va s'occuper des invités. Pendant la fête, elle retourne voir Simon et s'aperçoit qu'il a disparu.</p>",
    'acteurs' => ['Belén Rueda', 'Fernando Cayo', 'Roger Prìncep', 'Mabel Rivera', 'Montserrat Carulla', 'Andrés Gertrúdix', 'Edgar Vivar', 'Geraldine Chaplin']
];

$videotheque[] = [
    'nom' => 'L\'homme bicentenaire',
    'date' => 2000,
    'realisateur' => 'Chris Columbus',
    'synopsis' => "<p>En ce début de XXIe siècle, où le progrès technique s'est généralisé pour le meilleur, Richard Martin fait l'acquisition du tout nouveau robot domestique à la mode, le NDR-114. Celui-ci a été conçu pour effectuer les tâches ménagères pénibles jadis dévolues aux êtres humains : cuisine, ménage, bricolage, surveillance des enfants. Il est baptisé Andrew.</p>

    <p>À ceci près que l'« Andrew » (Amanda entend mal le mot \"androïde\") en question ne sera pas aussi simple que le modèle classique. Doté d'un esprit d'analyse modifié par accident, le robot se montre capable de créativité (notamment en sculptant de petits animaux en bois sans que personne ne lui apprenne). Il va se donner des buts, des objectifs à atteindre et va, au fil de ses apprentissages et de ses émotions, apprendre la vie.</p>

    <p>Andrew ira même jusqu'à défier la mort de l'Homme en l'aidant à survivre à ses propres peurs. Mais il découvrira très vite que la vie et l'amour ont un prix, dont lui seul pourra déterminer le montant.</p>",
    'acteurs' => ['Robin Williams', 'Embeth Davidtz', 'Sam Neill']
];

$videotheque[] = [
    'nom' => 'Zootopie',
    'date' => 2016,
    'realisateur' => 'Byron Howard, Rich Moore, Jared Bush',
    'synopsis' => "<p>Zootopie est une ville cosmopolite où ne vivent que des mammifères et où chaque espèce cohabite avec les autres ; qu’on soit un prédateur ou une proie, tout le monde est accepté à Zootopie. Judy Hopps est une lapine de 9 ans qui vit à la campagne avec ses parents. Alors que tous les membres de sa famille sont cultivateurs, Judy annonce pendant son spectacle d'école qu'elle veut vivre à Zootopie la grande métropole et devenir officier de police, ce qui affole ses parents qui estiment ce métier trop dangereux pour un lapin. En sortant du spectacle, Judy intervient en voyant Gideon Grey, un renard voyou, en train de racketter ses camarades. Gideon réagit violemment, griffant Judy à la joue, et se moque de son rêve de devenir lapin flic avant de s'en aller. Judy en sort traumatisée, mais encore plus déterminée à combattre le crime.</p>",
    'acteurs' => ['Ginnifer Goodwin', 'Jason Bateman', 'Idris Elba', 'J. K. Simmons', 'Jenny Slate']
];

foreach ($videotheque as $film) {
    foreach ($film as $cle => $info) {
        if (is_array($info)) {
            echo "$cle : ";
            foreach ($info as $item) {
                echo "$item, ";
            }
        } else {
            echo "$cle : $info<br>";
        }
    }
    echo "<br><br>";
}

echo '<br><br>';
